<div class="flex flex-col" wire:poll.3000ms>
@foreach ($categories as $category)
  <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
      <div class="p-2 shadow overflow-hidden border-b border-gray-200 sm:rounded-lg bg-gray-50">
        <h2 class="text-center font-medium text-xl">{{ $category->title }}</h2>
        @if ($category->menus->count())
        <table class="mt-2 min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 w-3/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Image
              </th>
              <th scope="col" class="px-6 py-3 w-5/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Title
              </th>
              <th scope="col" class="px-6 py-3 w-2/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Price
              </th>
              <th scope="col" class="relative w-2/12 px-6 py-3">
                Actions
              </th>
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            @foreach ($category->menus as $menu)
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        {{ $menu->preview->url ? $menu->preview->url : 'No Preview' }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        {{ $menu->title }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        {{ $menu->price }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-center text-sm font-medium">
                        <button wire:click="order({{ $menu->id }})" class="w-full sm:w-auto py-2 px-3 bg-gray-300 text-gray-900 hover:bg-gray-100 disabled:bg-gray-50 text-sm font-semibold rounded-md shadow focus:outline-none cursor-pointer">
                            Order
                        </button>
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
        @else
            <h3 class="mt-2 text-center font-medium text-gray-500 tracking-wider">
                Блюда временно отсутвтуют
            </h3>
        @endif
      </div>
    </div>
  </div>
@endforeach
</div>