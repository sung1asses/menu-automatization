<div class="flex flex-col" wire:poll.3000ms>
    @foreach ($orders as $key => $order_list)
      <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div class="p-2 shadow overflow-hidden border-b border-gray-200 sm:rounded-lg bg-gray-50">
            <h2 class="text-center font-medium text-xl">{{ \App\Models\Order::status()[$key] }}</h2>
            @if ($order_list->count())
            <table class="mt-2 min-w-full divide-y divide-gray-200">
              <thead class="bg-gray-50">
                <tr>
                  <th scope="col" class="px-6 py-3 w-2/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Id
                  </th>
                  <th scope="col" class="px-6 py-3 w-4/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Menu Items
                  </th>
                  <th scope="col" class="relative w-4/12 px-6 py-3">
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($order_list as $order)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                            {{ $order->id }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                            {{ $order->menus->implode('title', ', ') }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-center text-sm font-medium">
                            <div class="inline-flex rounded-lg shadow-sm" role="group">
                                @foreach (\App\Models\Order::status() as $key => $title)
                                    <button wire:click="changeStatus({{ $order->id }}, {{ $key }})" type="button" class="py-2 px-4 text-sm font-medium {{ $key == $order->status ? 'bg-gray-600' : 'bg-gray-700' }} border-gray-600 text-white hover:text-white hover:bg-gray-600 focus:ring-blue-500 focus:text-white">
                                        {{ $title }}
                                    </button>
                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            @else
                <h3 class="mt-2 text-center font-medium text-gray-500 tracking-wider">
                    Заказы на сегодня отсутвтуют
                </h3>
            @endif
          </div>
        </div>
      </div>
    @endforeach
    </div>