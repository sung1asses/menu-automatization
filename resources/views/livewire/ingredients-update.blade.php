<div class="flex justify-between">
  <div class="flex flex-col px-4">
    <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
          <input type="text" wire:model="search" placeholder="Search" name="search" id="search" class="focus:ring-gray-100 focus:border-gray-100 block w-full pl-7 pr-12 sm:text-sm border-gray-300 rounded-md">
        </div>
      </div>
    </div>
    <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
          @if ($ingredients->count())
          <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
              <tr>
                <th scope="col" class="px-6 py-3 w-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Ingredient
                </th>
                <th scope="col" class="px-6 py-3 w-20 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Count
                </th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
              @foreach ($ingredients as $ingredient)
                <tr wire:click="choose({{ $ingredient->id }})" class="cursor-pointer hover:bg-gray-100">
                  <td class="px-6 py-4 whitespace-nowrap">
                    {{ $ingredient->title }}
                  </td>
                  <td class="px-6 py-4 whitespace-nowrap text-sm">
                    {{ $ingredient->unit_count }}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          @else
            <div class="px-6 py-3 bg-gray-50">
              Таких ингредиентов мы не знаем...
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="flex flex-col px-4">
    @if ($choosed_ingredients)
    <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
          @foreach ($choosed_ingredients as $key => $ch_ing)
          <div class="px-6 py-3 bg-white">
            <label for="price" class="block text-sm font-medium text-gray-700">{{ $ingredients->find($ch_ing['id'])->title }}</label>
            <div class="mt-1 relative rounded-md shadow-sm">
              <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span class="text-gray-500 sm:text-sm">
                  шт
                </span>
              </div>
              <input type="text" wire:model="choosed_ingredients.{{ $key }}.count" class="focus:ring-gray-100 focus:border-gray-100 block w-full pl-10 pr-12 sm:text-sm border-gray-300 rounded-md">
            </div>
            <div class="mt-1 relative rounded-md shadow-sm">
              <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span class="text-gray-500 sm:text-sm">
                  Тг
                </span>
              </div>
              <input type="text" wire:model="choosed_ingredients.{{ $key }}.price" class="focus:ring-gray-100 focus:border-gray-100 block w-full pl-10 pr-12 sm:text-sm border-gray-300 rounded-md">
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <button wire:click="update" class="ml-auto w-full sm:w-auto py-2 px-3 bg-gray-300 text-gray-900 hover:bg-gray-100 disabled:bg-gray-50 text-sm font-semibold rounded-md shadow focus:outline-none cursor-pointer">
          Update
        </button>
      </div>
    </div>
    @endif
  </div>
</div>