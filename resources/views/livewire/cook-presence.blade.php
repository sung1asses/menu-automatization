<div class="flex flex-col">
@foreach ($cooking_types as $type)
  <div class="py-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
      <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
        <table class="min-w-full divide-y divide-gray-200">
          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 w-5/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Name
              </th>
              <th scope="col" class="px-6 py-3 w-4/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Proffession
              </th>
              <th scope="col" class="px-6 py-3 w-2/12 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Presence
              </th>
              <th scope="col" class="px-6 py-3 w-1/12 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                Checkout
              </th>
            </tr>
          </thead>
          <tbody class="bg-white divide-y divide-gray-200">
            @foreach ($type->cooks as $cook)
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-sm font-medium text-gray-900">
                        {{ $cook->name }}
                    </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                    {{ $cook->proffession }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        @if ($cook->is_present)
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                {{ $cook->presence }}
                            </span>
                        @else
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800">
                                {{ $cook->presence }}
                            </span>
                        @endif
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <input name="cook_presence_{{ $cook->id }}" wire:click="toggle_present({{ $cook->id }})" type="checkbox" {{ $cook->is_present?'checked':'' }}>
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endforeach
</div>