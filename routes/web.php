<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('menu');
})->name('menu');
Route::middleware('auth')->group(function () {
    Route::get('/cook_presence', function () {
        return view('cook-presence');
    })->name('cook-presence');
    Route::get('/ingredients_update', function () {
        return view('ingredients-update');
    })->name('ingredients-update');
    Route::get('/orders', function () {
        return view('orders');
    })->name('orders');
});
