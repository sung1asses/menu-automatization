<?php

declare(strict_types=1);

use App\Orchid\Screens\Category\CategoryEditScreen;
use App\Orchid\Screens\Category\CategoryListScreen;
use App\Orchid\Screens\Cook\CookEditScreen;
use App\Orchid\Screens\Cook\CookListScreen;
use App\Orchid\Screens\CookingType\CookingTypeEditScreen;
use App\Orchid\Screens\CookingType\CookingTypeListScreen;
use App\Orchid\Screens\Ingredient\IngredientEditScreen;
use App\Orchid\Screens\Ingredient\IngredientListScreen;
use App\Orchid\Screens\Menu\MenuEditScreen;
use App\Orchid\Screens\Menu\MenuListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });
    
/////////////////////////////////////////
// Platform > Menu Categories > Category
Route::screen('categories/{category}/edit', CategoryEditScreen::class)
    ->name('platform.categories.edit');

// Platform > Menu Categories > Create
Route::screen('categories/create', CategoryEditScreen::class)
    ->name('platform.categories.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.categories')
            ->push(__('Create'), route('platform.categories.create'));
    });

// Platform > Menu Categories
Route::screen('categories', CategoryListScreen::class)
    ->name('platform.categories')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Menu Categories'), route('platform.categories'));
    });
/////////////////////////////////////////
/////////////////////////////////////////
// Platform > Ingredients > Ingredient
Route::screen('ingredients/{ingredient}/edit', IngredientEditScreen::class)
    ->name('platform.ingredients.edit');

// Platform > Ingredients > Create
Route::screen('ingredients/create', IngredientEditScreen::class)
    ->name('platform.ingredients.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.ingredients')
            ->push(__('Create'), route('platform.ingredients.create'));
    });

// Platform > Ingredients
Route::screen('ingredients', IngredientListScreen::class)
    ->name('platform.ingredients')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Ingredients'), route('platform.ingredients'));
    });
/////////////////////////////////////////
/////////////////////////////////////////
// Platform > Cooking Types > CookingType
Route::screen('cooking_types/{cooking_type}/edit', CookingTypeEditScreen::class)
    ->name('platform.cooking_types.edit');

// Platform > Cooking Types > Create
Route::screen('cooking_types/create', CookingTypeEditScreen::class)
    ->name('platform.cooking_types.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.cooking_types')
            ->push(__('Create'), route('platform.cooking_types.create'));
    });

// Platform > Cooking Types
Route::screen('cooking_types', CookingTypeListScreen::class)
    ->name('platform.cooking_types')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Cooking Types'), route('platform.cooking_types'));
    });
/////////////////////////////////////////
/////////////////////////////////////////
// Platform > Cooks > Cook
Route::screen('cooks/{cook}/edit', CookEditScreen::class)
    ->name('platform.cooks.edit');

// Platform > Cooks > Create
Route::screen('cooks/create', CookEditScreen::class)
    ->name('platform.cooks.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.cooks')
            ->push(__('Create'), route('platform.cooks.create'));
    });

// Platform > Cooks
Route::screen('cooks', CookListScreen::class)
    ->name('platform.cooks')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Cooks'), route('platform.cooks'));
    });
/////////////////////////////////////////
/////////////////////////////////////////
// Platform > Menu Items > Menu
Route::screen('menus/{menu}/edit', MenuEditScreen::class)
    ->name('platform.menus.edit');

// Platform > Menu Items > Create
Route::screen('menus/create', MenuEditScreen::class)
    ->name('platform.menus.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.menus')
            ->push(__('Create'), route('platform.menus.create'));
    });

// Platform > Menu Items
Route::screen('menus', MenuListScreen::class)
    ->name('platform.menus')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Menu Items'), route('platform.menus'));
    });
/////////////////////////////////////////

//Route::screen('idea', 'Idea::class','platform.screens.idea');
