<?php

namespace Database\Factories;

use App\Models\Menu_Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Menu_Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
