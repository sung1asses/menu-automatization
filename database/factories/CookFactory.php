<?php

namespace Database\Factories;

use App\Models\Cook;
use Illuminate\Database\Eloquent\Factories\Factory;

class CookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cook::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name()
        ];
    }
}
