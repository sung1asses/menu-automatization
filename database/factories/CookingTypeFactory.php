<?php

namespace Database\Factories;

use App\Models\CookingType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CookingTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CookingType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
