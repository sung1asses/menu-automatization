<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Cook;
use App\Models\CookingType;
use App\Models\Ingredient;
use App\Models\Menu;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Artisan::call('orchid:admin admin admin@admin.com password');
        \App\Models\User::find(1)->update(['email_verified_at' => now()]);
        $cook_types = [
            'Кондитер',
            // 'Пекарь',
            'Пиццмейкер',
            'Повар',
            'Сушист',
            'Шашлычник',
        ];
        foreach($cook_types as $type){
            CookingType::factory()->has(
                Cook::factory()->count(3)
            )->createOne([
                'title' => $type
            ]);
        }
        $menu_types = [
            [
                'title' => 'Салаты',
                'cooking_type' => 'Повар',
                'menu_items' => [
                    [
                        'title' => 'Оливье',
                        'ingredients' => [
                            'Картофель',
                            'Морковь',
                            'Огурец маринованный',
                            'Лук репчатый',
                            'Колбаса',
                            'Горошек зеленый',
                            'Майонез',
                            'Мука пшеничная',
                            'Сыр твердый',
                            'Яйцо куриное',
                            'Масло растительное',
                        ]
                    ],
                    [
                        'title' => 'Цезарь',
                        'ingredients' => [
                            'Листья салата',
                            'Курица',
                            'Яйцо куриное',
                            'Сухарики',
                            'Помидоры черри',
                            'Сыр твердый',
                            'Соус',
                        ]
                    ],
                    [
                        'title' => 'Ананасовый',
                        'ingredients' => [
                            'Ананас',
                            'Яйцо куриное',
                            'Крабовые палочки',
                            'Сыр твердый',
                            'Капуста пекинская',
                            'Лук зеленый',
                            'Укроп',
                            'Майонез',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Первое',
                'cooking_type' => 'Повар',
                'menu_items' => [
                    [
                        'title' => 'Борщ',
                        'ingredients' => [
                            'Мясо',
                            'Картофель',
                            'Свекла',
                            'Морковь',
                            'Капуста белокочанная',
                            'Помидор',
                            'Уксус',
                            'Сахар',
                            'Соль',
                            'Укроп',
                            'Чеснок',
                        ]
                    ],
                    [
                        'title' => 'Гороховый суп',
                        'ingredients' => [
                            'Бекон',
                            'Морковь',
                            'Лук репчатый',
                            'Горох',
                            'Соль',
                            'Масло сливочное',
                        ]
                    ],
                    [
                        'title' => 'Харчо',
                        'ingredients' => [
                            'Баранина',
                            'Рис',
                            'Лук репчатый',
                            'Помидор',
                            'Чеснок',
                            'Ткемали',
                            'Соль',
                            'Кинза',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Второе',
                'cooking_type' => 'Повар',
                'menu_items' => [
                    [
                        'title' => 'Мясо по французски',
                        'ingredients' => [
                            'Мясо',
                            'Картофель',
                            'Лук репчатый',
                            'Перец болгарский',
                            'Баклажан',
                            'Помидор',
                            'Майонез',
                            'Сыр твердый',
                            'Приправа',
                        ]
                    ],
                    [
                        'title' => 'Стейк из говядины',
                        'ingredients' => [
                            'Говядина',
                            'Масло сливочное',
                            'Соль',
                            'Перец черный',
                        ]
                    ],
                    [
                        'title' => 'Плов',
                        'ingredients' => [
                            'Бастурма',
                            'Морковь',
                            'Лук репчатый',
                            'Масло растительное',
                            'Соль',
                            'Рис',
                            'Вода',
                            'Чеснок',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Пицца',
                'cooking_type' => 'Пиццмейкер',
                'menu_items' => [
                    [
                        'title' => 'Маргарита',
                        'ingredients' => [
                            'Мука пшеничная',
                            'Помидор',
                            'Сыр полутвердый',
                            'Базилик',
                            'Масло оливковое',
                            'Перец душистый',
                            'Соль',
                            'Вода',
                            'Дрожжи',
                            'Сахар',
                        ]
                    ],
                    [
                        'title' => 'Гавайская',
                        'ingredients' => [
                            'Мука пшеничная',
                            'Молоко',
                            'Сахар',
                            'Соль',
                            'Дрожжи',
                            'Яйцо куриное',
                            'Масло оливковое',
                            'Кетчуп',
                            'Сыр твердый',
                            'Грудка куриная',
                            'Перец сладкий красный',
                            'Ананас',
                        ]
                    ],
                    [
                        'title' => 'Мясной Рай',
                        'ingredients' => [
                            'Вода',
                            'Дрожжи',
                            'Сахар',
                            'Мука пшеничная',
                            'Ряженка',
                            'Соль',
                            'Масло оливковое',
                            'Помидор',
                            'Орегано',
                            'Базилик',
                            'Соевый соус',
                            'Чеснок',
                            'Салями',
                            'Буженина',
                            'Курица',
                            'Сыр голландский',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Суши',
                'cooking_type' => 'Сушист',
                'menu_items' => [
                    [
                        'title' => 'Дзакуро',
                        'ingredients' => [
                            'Рис',
                            'Нори',
                            'Лосось',
                            'Яйцо куриное',
                            'Желток яичный',
                            'Соевый соус',
                            'Сахар',
                            'Соль',
                            'Масло растительное',
                            'Кунжут',
                            'Уксус',
                        ]
                    ],
                    [
                        'title' => 'Арабески',
                        'ingredients' => [
                            'Рис',
                            'Нори',
                            'Икра рыбная',
                            'Уксус',
                        ]
                    ],
                    [
                        'title' => 'Сакура',
                        'ingredients' => [
                            'Креветки',
                            'Икра красная',
                            'Сыр твердый',
                            'Рис',
                            'Соевый соус',
                            'Васаби',
                            'Имбирь',
                            'Сахар',
                            'Соль',
                            'Уксус',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Шашлыки',
                'cooking_type' => 'Шашлычник',
                'menu_items' => [
                    [
                        'title' => 'Свинина',
                        'ingredients' => [
                            'Свинина',
                            'Кефир',
                            'Лук репчатый',
                            'Соль',
                            'Перец черный',
                            'Приправа',
                        ]
                    ],
                    [
                        'title' => 'Говядина',
                        'ingredients' => [
                            'Говядина',
                            'Брусника',
                            'Соевый соус',
                            'Масло оливковое',
                            'Мята',
                            'Смесь перцев',
                            'Паприка сладкая',
                        ]
                    ],
                    [
                        'title' => 'Баранина',
                        'ingredients' => [
                            'Баранина',
                            'Лук репчатый',
                            'Соус-маринад',
                            'Аджика',
                            'Уксус',
                            'Лимон',
                            'Горчица',
                            'Соль',
                        ]
                    ],
                ]
            ],
            [
                'title' => 'Десерт',
                'cooking_type' => 'Кондитер',
                'menu_items' => [
                    [
                        'title' => 'Пирожное "Ленинградское"',
                        'ingredients' => [
                            'Вода',
                            'Молоко',
                            'Масло сливочное',
                            'Мука пшеничная',
                            'Яйцо куриное',
                            'Соль',
                            'Сахар коричневый',
                            'Ванилин',
                            'Тесто слоеное бездрожжевое',
                        ]
                    ],
                    [
                        'title' => 'Пончик',
                        'ingredients' => [
                            'Молоко',
                            'Дрожжи',
                            'Сахар',
                            'Мука пшеничная',
                            'Желток яичный',
                            'Масло сливочное',
                            'Соль',
                            'Масло подсолнечное',
                            'Сахарная пудра',
                        ]
                    ],
                    [
                        'title' => 'Чизкейк',
                        'ingredients' => [
                            'Печенье',
                            'Масло сливочное',
                            'Творог',
                            'Сливки',
                            'Сахар',
                            'Яйцо куриное',
                            'Ванильный сахар',
                            'Ванилин',
                            'Малина',
                            'Сметана',
                        ]
                    ],
                ]
            ],
        ];
        foreach($menu_types as $type){
            $category = Category::create([
                'title' => $type['title']
            ]);
            $cooking_type = CookingType::where('title', $type['cooking_type'])->first();
            foreach($type['menu_items'] as $menu){
                $menu_model = Menu::create([
                    'title' => $menu['title'],
                    'time_to_cook' => rand(10, 60),
                    'category_id' => $category->id,
                ]);
                $menu_model->cooking_types()->syncWithoutDetaching([$cooking_type->id]);
                foreach($menu['ingredients'] as $ingredient){
                    $ingredient = Ingredient::firstOrCreate(
                        ['title' => $ingredient],
                        ['unit_count' => rand(100, 200)],
                    );
                    $ingredient->ingredient_prices()->create([
                        'price' => rand(1, 300)
                    ]);
                    $id = $ingredient->id;
                    $menu_model->ingredients()->syncWithoutDetaching([$id => [
                        'unit_count' => rand(4, 20)
                    ]]);
                }
            }
        }
    }
}
