<?php

namespace App\Http\Livewire;

use App\Models\Cook;
use App\Models\CookingType;
use Livewire\Component;

class CookPresence extends Component
{
    public function toggle_present(Cook $cook){
        $cook->update([
            'is_present' => !$cook->is_present
        ]);
    }

    private function getCookingTypes(){
        return CookingType::with('cooks')->get();
    }

    public function render()
    {
        return view('livewire.cook-presence', [
            'cooking_types' => $this->getCookingTypes()
        ]);
    }
}
