<?php

namespace App\Http\Livewire;

use App\Models\Ingredient;
use Livewire\Component;

class IngredientsUpdate extends Component
{
    public $choosed_ingredients = [];

    public $search = "";

    public function choose($id){
        $filtering = array_search($id, array_column($this->choosed_ingredients, 'id'));
        if( is_int($filtering) ){
            unset($this->choosed_ingredients[$filtering]);
            $this->choosed_ingredients = array_merge($this->choosed_ingredients);
        }
        else{
            $this->choosed_ingredients[] = [
                'id' => $id,
                'count' => null,
                'price' => Ingredient::find($id)->price,
            ];
        }
    }

    private function getIngredients(){
        return Ingredient::when($this->search, function ($q){
            return $q->where('title', 'like', '%'.$this->search.'%');
        })->orderBy('unit_count')->get();
    }

    public function update(){
        foreach($this->choosed_ingredients as $ch_ing){
            $ing_to_update = Ingredient::find($ch_ing['id']);
            $ing_to_update->update([
                'unit_count' => $ing_to_update->unit_count+ intval($ch_ing['count'])
            ]);
            if($ing_to_update->price != intval( $ch_ing['price'] )){
                $ing_to_update->ingredient_prices()->create([
                    'price' => intval( $ch_ing['price'] )
                ]);
            }
        }
        $this->choosed_ingredients = [];
    }

    public function render()
    {
        return view('livewire.ingredients-update', [
            'ingredients' => $this->getIngredients(),
        ]);
    }
}
