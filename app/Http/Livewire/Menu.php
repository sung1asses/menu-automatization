<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Menu as ModelsMenu;
use App\Models\Order;
use Livewire\Component;

class Menu extends Component
{
    public function order($id){
        $menu = ModelsMenu::find($id);
        $order = Order::create();
        $order->menus()->sync([
            $id => [
                'total_price' => $menu->price
            ]
        ]);
        $ingredients = $menu->ingredients()->get();
        foreach($ingredients as $ingredient){
            $ingredient->update([
                'unit_count' => $ingredient->unit_count - $ingredient->pivot->unit_count
            ]);
        }
    }

    public function render()
    {
        return view('livewire.menu', [
            'categories' => Category::with([
                'menus' => function ($menu){
                    return $menu
                        ->withCount([
                            'cooking_types' => function ($cooking_type){
                                return $cooking_type->whereRelation('cooks', 'cooks.is_present', true);
                            },
                            'ingredients' => function ($ingredient){
                                return $ingredient->whereColumn('ingredients.unit_count', '<', 'ingredient__menu.unit_count');
                            }
                        ])
                        ->having('cooking_types_count', '!=', 0)
                        ->having('ingredients_count', '==', 0)
                        ;
                },
            ])->get()
        ]);
    }
}
