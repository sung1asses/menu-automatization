<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Livewire\Component;

class Orders extends Component
{
    public function changeStatus($id, $status_key){
        Order::find($id)->update([
            'status' => $status_key
        ]);
    }

    public function render()
    {
        return view('livewire.orders', [
            'orders' => [
                0 => Order::where([
                    ['status', 0],
                    ['created_at', '>=',now()->subDay()],
                ])->get(),
                1 => Order::where([
                    ['status', 1],
                    ['created_at', '>=',now()->subDay()],
                ])->get(),
                2 => Order::where([
                    ['status', 2],
                    ['created_at', '>=',now()->subDay()],
                ])->get(),
            ],
        ]);
    }
}
