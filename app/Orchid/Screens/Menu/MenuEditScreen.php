<?php

namespace App\Orchid\Screens\Menu;

use App\Models\Category;
use App\Models\CookingType;
use App\Models\Ingredient;
use Illuminate\Http\Request;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Cropper;

use App\Models\Menu;
use App\Orchid\Fields\Slug;
use App\Orchid\Layouts\MenuIngredientListener;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;

class MenuEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit Menu';

    /**
     * @var Menu
     */
    private $menu;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Menu $menu): array
    {
        $this->menu = $menu;
        if (! $menu->exists) {
            $this->name = 'Create Menu';
        }
        return [
            'menu' => $menu,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                ->method('remove')
                ->canSee($this->menu->exists),

            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    public function asyncIngredients($data)
    {
        $ingredients_ids = explode(',', $data['ingredients'][0]);
        return [
            'ingredients' => Ingredient::find($ingredients_ids),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Cropper::make('menu.preview_id')
                        ->title('Картинка Блюда')
                        ->targetId()
                        ->width(400)
                        ->height(400),
                    Input::make('menu.title')
                        ->title('Заголовок')
                        ->placeholder(''),
                    Input::make('menu.time_to_cook')
                        ->title('Время приготовления (мин)')
                        ->type('integer')
                        ->placeholder(''),
                    Relation::make('menu.category_id')
                        ->title('Категория')
                        ->fromModel(Category::class, 'title'),
                    Relation::make('menu.cooking_types.')
                        ->title('Ответственные')
                        ->multiple()
                        ->fromModel(CookingType::class, 'title'),
                ]),
            ]),
            MenuIngredientListener::class,
        ];
    }
    
    /**
     * @param Menu    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Menu $menu, Request $request)
    {
        $request->validate([
            'menu.preview_id' => 'required',
            'menu.title' => 'required|unique:menus,title'.( $menu->exists ? ','.$menu->id : '' ),
            'menu.time_to_cook' => 'required',
            'menu.category_id' => 'required',
            'menu.ingredients' => 'required',
            'menu.ingredients_unit_count' => 'required',
            'menu.cooking_types' => 'required',
        ]);

        $menuData = $request->get('menu');

        $menu
            ->fill($menuData)
            ->save();
            
        $ingredient_ids = $request->input('menu.ingredients', []);
        $ingredient_id_count = $request->input('menu.ingredients_unit_count', []);
        $ingredients_with_count = $ingredient_ids->mapWithKeys(function ($ingredient_id, $key) use ($ingredient_id_count){
            return [$ingredient_id => $ingredient_id_count[$ingredient_id]];
        });
        $menu->ingredients()->sync($ingredients_with_count);
        $menu->cooking_types()->sync($request->input('menu.cooking_types', []));

        Toast::info(__('Menu was saved.'));

        return redirect()->route('platform.menus');
    }
}
