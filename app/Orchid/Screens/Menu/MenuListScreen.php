<?php

namespace App\Orchid\Screens\Menu;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\TD;

use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;

use Orchid\Support\Facades\Layout;

use Orchid\Support\Facades\Toast;

use App\Models\Menu;

class MenuListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Menus';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            // 'menus' => App\Models\Menu::when($search, function ($query, $search) {
            //     return $query->where('title', 'like', $search.'%');
            // })
            // ->paginate(5),
            'menus' => Menu::defaultSort('id', 'desc')->filters()->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.menus.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('menus', [
                TD::make('title')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
                TD::make('time_to_cook', 'Time to cook')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
                TD::make('category_id', 'Category')
                    ->render(function ($menu){
                        return $menu->category->title;
                    }),
                TD::make('ingredients')
                    ->render(function ($menu){
                        return $menu->ingredients->pluck('title')->implode(',<br>');
                    }),
                TD::make('cooking_types', 'Cooking Types')
                    ->render(function ($menu){
                        return $menu->cooking_types->pluck('title')->implode(',<br>');
                    }),
                TD::make('created_at')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function ($model){
                        return $model->created_at->isoFormat('LLL');
                    }),
                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Menu $menu) {
                        $isDeletable = true;
                        $list = [
                            Link::make(__('Edit'))
                            ->route('platform.menus.edit', $menu->id)
                            ->icon('pencil'),
                        ];
                        if($isDeletable){
                            $list[] = Button::make(__('Delete'))
                                        ->icon('trash')
                                        ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                                        ->method('remove', [
                                            'id' => $menu->id,
                                        ]);
                        }
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list($list);
                    }),
            ])
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Menu::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('Menu was removed'));
    }
}
