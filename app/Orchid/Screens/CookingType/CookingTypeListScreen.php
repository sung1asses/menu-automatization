<?php

namespace App\Orchid\Screens\CookingType;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\TD;

use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;

use Orchid\Support\Facades\Layout;

use Orchid\Support\Facades\Toast;

use App\Models\CookingType;

class CookingTypeListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CookingTypes';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            // 'cooking_types' => App\Models\CookingType::when($search, function ($query, $search) {
            //     return $query->where('title', 'like', $search.'%');
            // })
            // ->paginate(5),
            'cooking_types' => CookingType::defaultSort('id', 'desc')->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.cooking_types.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('cooking_types', [
                TD::make('title'),
                TD::make('created_at')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function ($model){
                        return $model->created_at->isoFormat('LLL');
                    }),
                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (CookingType $cooking_type) {
                        $isDeletable = true;
                        $list = [
                            Link::make(__('Edit'))
                            ->route('platform.cooking_types.edit', $cooking_type->id)
                            ->icon('pencil'),
                        ];
                        if($isDeletable){
                            $list[] = Button::make(__('Delete'))
                                        ->icon('trash')
                                        ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                                        ->method('remove', [
                                            'id' => $cooking_type->id,
                                        ]);
                        }
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list($list);
                    }),
            ])
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        CookingType::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('CookingType was removed'));
    }
}
