<?php

namespace App\Orchid\Screens\CookingType;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Cropper;

use App\Models\CookingType;
use App\Orchid\Fields\Slug;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;

class CookingTypeEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit CookingType';

    /**
     * @var CookingType
     */
    private $cooking_type;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(CookingType $cooking_type): array
    {
        $this->cooking_type = $cooking_type;
        if (! $cooking_type->exists) {
            $this->name = 'Create CookingType';
        }
        return [
            'cooking_type' => $cooking_type,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                ->method('remove')
                ->canSee($this->cooking_type->exists),

            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('cooking_type.title')
                        ->title('Заголовок')
                        ->placeholder(''),
                ])
            ])
        ];
    }
    
    /**
     * @param CookingType    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(CookingType $cooking_type, Request $request)
    {
        $request->validate([
            'cooking_type.title' => 'required|unique:cooking_types,title'.( $cooking_type->exists ? ','.$cooking_type->id : '' ),
        ]);

        $cooking_typeData = $request->get('cooking_type');

        $cooking_type
            ->fill($cooking_typeData)
            ->save();

        Toast::info(__('CookingType was saved.'));

        return redirect()->route('platform.cooking_types');
    }
}
