<?php

namespace App\Orchid\Screens\Ingredient;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Cropper;

use App\Models\Ingredient;
use App\Models\IngredientPrice;
use App\Orchid\Fields\Slug;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;

class IngredientEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit Ingredient';

    /**
     * @var Ingredient
     */
    private $ingredient;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Ingredient $ingredient): array
    {
        $this->ingredient = $ingredient;
        if (! $ingredient->exists) {
            $this->name = 'Create Ingredient';
        }
        return [
            'ingredient' => $ingredient,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                ->method('remove')
                ->canSee($this->ingredient->exists),

            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('ingredient.title')
                        ->title('Заголовок')
                        ->placeholder(''),
                    Input::make('ingredient.unit_count')
                        ->title('Кол-во юнитов')
                        ->type('number')
                        ->placeholder('')
                        ->canSee(!$this->ingredient->exists),
                    Input::make('ingredient.price')
                        ->title('Цена')
                        ->type('number')
                        ->placeholder('')
                        ->canSee(!$this->ingredient->exists),
                ])
            ])
        ];
    }
    
    /**
     * @param Ingredient    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Ingredient $ingredient, Request $request)
    {
        $is_update = $ingredient->exists;
        $request->validate([
            'ingredient.title' => 'required|unique:ingredients,title'.( $is_update ? ','.$ingredient->id : '' ),
            'ingredient.unit_count' => ( !$is_update ? 'required' : '' ),
            'ingredient.price' => ( !$is_update ? 'required' : '' ),
        ]);

        $ingredientData = $request->get('ingredient');
        
        $ingredient
            ->fill($ingredientData)
            ->save();

        if(!$is_update){
            $ingredient->ingredient_prices()->create([
                'price' => $ingredientData['price']
            ]);
        }

        Toast::info(__('Ingredient was saved.'));

        return redirect()->route('platform.ingredients');
    }
}
