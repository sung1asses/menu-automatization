<?php

namespace App\Orchid\Screens\Ingredient;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\TD;

use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;

use Orchid\Support\Facades\Layout;

use Orchid\Support\Facades\Toast;

use App\Models\Ingredient;

class IngredientListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Ingredients';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            // 'ingredients' => App\Models\Ingredient::when($search, function ($query, $search) {
            //     return $query->where('title', 'like', $search.'%');
            // })
            // ->paginate(5),
            'ingredients' => Ingredient::defaultSort('id', 'desc')->filters()->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.ingredients.create'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('ingredients', [
                TD::make('title')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
                TD::make('unit_count', 'Кол-во юнитов')
                    ->sort()
                    ->filter(TD::FILTER_NUMERIC),
                TD::make('price', 'Текущая цена')
                    ->sort()
                    ->filter(TD::FILTER_NUMERIC),
                TD::make('created_at')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function ($model){
                        return $model->created_at->isoFormat('LLL');
                    }),
                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Ingredient $ingredient) {
                        $isDeletable = true;
                        $list = [
                            Link::make(__('Edit'))
                            ->route('platform.ingredients.edit', $ingredient->id)
                            ->icon('pencil'),
                        ];
                        if($isDeletable){
                            $list[] = Button::make(__('Delete'))
                                        ->icon('trash')
                                        ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                                        ->method('remove', [
                                            'id' => $ingredient->id,
                                        ]);
                        }
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list($list);
                    }),
            ])
        ];
    }


    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        Ingredient::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('Ingredient was removed'));
    }
}
