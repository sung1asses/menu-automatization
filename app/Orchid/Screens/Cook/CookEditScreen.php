<?php

namespace App\Orchid\Screens\Cook;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

use Orchid\Screen\Fields\Input;

use App\Models\Cook;
use App\Models\CookingType;
use Orchid\Screen\Fields\Relation;

class CookEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit Cook';

    /**
     * @var Cook
     */
    private $cook;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Cook $cook): array
    {
        $this->cook = $cook;
        if (! $cook->exists) {
            $this->name = 'Create Cook';
        }
        return [
            'cook' => $cook,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                ->method('remove')
                ->canSee($this->cook->exists),

            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('cook.name')
                        ->title('Имя')
                        ->placeholder(''),
                    Relation::make('cook.cooking_type_id')
                        ->title('Профессия')
                        ->fromModel(CookingType::class, 'title')
                        ->placeholder(''),
                ])
            ])
        ];
    }
    
    /**
     * @param Cook    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Cook $cook, Request $request)
    {
        $request->validate([
            'cook.name' => 'required|unique:cooks,name'.( $cook->exists ? ','.$cook->id : '' ),
        ]);

        $cookData = $request->get('cook');

        $cook
            ->fill($cookData)
            ->save();

        Toast::info(__('Cook was saved.'));

        return redirect()->route('platform.cooks');
    }
}
