<?php

namespace App\Orchid\Screens\Category;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

use Orchid\Screen\Fields\Input;

use App\Models\Category;

class CategoryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Edit Category';

    /**
     * @var Category
     */
    private $category;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Category $category): array
    {
        $this->category = $category;
        if (! $category->exists) {
            $this->name = 'Create Category';
        }
        return [
            'category' => $category,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
                ->method('remove')
                ->canSee($this->category->exists),

            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('category.title')
                        ->title('Заголовок')
                        ->placeholder(''),
                ])
            ])
        ];
    }
    
    /**
     * @param Category    $user
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Category $category, Request $request)
    {
        $request->validate([
            'category.title' => 'required|unique:categories,title'.( $category->exists ? ','.$category->id : '' ),
        ]);

        $categoryData = $request->get('category');

        $category
            ->fill($categoryData)
            ->save();

        Toast::info(__('Category was saved.'));

        return redirect()->route('platform.categories');
    }
}
