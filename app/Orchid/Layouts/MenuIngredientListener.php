<?php

namespace App\Orchid\Layouts;

use App\Models\Ingredient;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Layouts\Listener;

class MenuIngredientListener extends Listener
{   
    /**
     * List of field names for which values will be listened.
     *
     * @var string[]
     */
    protected $targets = ['menu.ingredients.'];

    /**
     * What screen method should be called
     * as a source for an asynchronous request.
     *
     * The name of the method must
     * begin with the prefix "async"
     *
     * @var string
     */
    protected $asyncMethod = 'asyncIngredients';

    /**
     * @return Layout[]
     */
    protected function layouts(): array
    {
        $ingredients = $this->query->get('ingredients') 
            ? $this->query->get('ingredients') 
            : $this->query->get('menu')->ingredients;
        $ingredients_unit_count = $this->query->get('ingredients_unit_count') 
            ? $this->query->get('ingredients_unit_count') 
            : $ingredients->mapWithKeys(function ($ingredient, $key){
                return [$ingredient->id => $ingredient->pivot ? $ingredient->pivot->unit_count : null];
            });
        $input_array = [];
        if($ingredients){
            foreach($ingredients_unit_count as $ingredient_id => $count){
                $input_array[] = Input::make('menu.ingredients_unit_count.'.$ingredient_id)
                                    ->title('Кол-во: '.$ingredients->find($ingredient_id)->title)
                                    ->value($count);
            }
        }
        return [
                Layout::columns([
                    Layout::rows([
                        Relation::make('menu.ingredients.')
                            ->title('Ингредиенты')
                            ->multiple()
                            ->value($ingredients ? $ingredients->pluck('id')->toArray() : [])
                            ->fromModel(Ingredient::class, 'title'),
                    ]),
                    Layout::rows($input_array),
                ])
            ];
    }
}
