<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class CookingType extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];

    protected $with = [
    ];

    public function cooks(){
        return $this->hasMany(Cook::class);
    }

    public function menus(){
        return $this->belongsToMany(Menu::class, 'cooking_type__menu')->using(CookingType_Menu::class);
    }
}
