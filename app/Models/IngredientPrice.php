<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class IngredientPrice extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'price',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'price',
        'created_at',
        'updated_at',
    ];
    
    public function ingredient(){
        return $this->belongsTo(Ingredient::class);
    }
}
