<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Category extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'title',
        'created_at',
        'updated_at',
    ];

    public function menus(){
        return $this->hasMany(Menu::class);
    }

    public function active_menus(){
        // TODO
        return $this->hasMany(Menu::class);
    }
}
