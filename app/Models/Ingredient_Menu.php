<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Ingredient_Menu extends Pivot
{
    public $incrementing = true;
}
