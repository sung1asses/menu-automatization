<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Ingredient extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'title',
        'unit_count',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'title',
        'unit_count',
        'created_at',
        'updated_at',
    ];

    public function getPriceAttribute(){
        $history_price = $this->ingredient_prices()->orderBy('created_at', 'desc')->first();
        return $history_price ? $history_price->price : 0;
        //Если 0, то цена ещё не установлена
    }

    public function menus(){
        return $this->belongsToMany(Menu::class, 'ingredient__menu')->withPivot('unit_count')->using(Ingredient_Menu::class);
    }

    public function ingredient_prices(){
        return $this->hasMany(IngredientPrice::class);
    }
}
