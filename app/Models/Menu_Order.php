<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Menu_Order extends Pivot
{
    public $incrementing = true;

    protected $with = [
        'cook',
    ];

    // size
    public function cook(){
        return $this->belongsTo(Cook::class);
    }
}
