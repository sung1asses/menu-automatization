<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Menu extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];
    
    protected $allowedFilters = [
        'id',
        'title',
        'time_to_cook',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'title',
        'time_to_cook',
        'created_at',
        'updated_at',
    ];

    protected $with = [
        'preview',
        'ingredients'
    ];

    public function getPriceAttribute(){
        return round($this->ingredients->sum(function ($ingredient) {
            return $ingredient->price;
        }), -2);
    }

    public function preview()
    {
        return $this->hasOne(Attachment::class, 'id', 'preview_id')->withDefault();
    }
    
    public function category(){
        return $this->belongsTo(Category::class);
    }
    
    public function ingredients(){
        return $this->belongsToMany(Ingredient::class, 'ingredient__menu')->withPivot('unit_count')->using(Ingredient_Menu::class);
    }
    
    public function orders(){
        return $this->belongsToMany(Order::class, 'menu__order')->withPivot('total_price','count','cook_id')->using(Menu_Order::class);
    }

    public function cooking_types(){
        return $this->belongsToMany(CookingType::class, 'cooking_type__menu')->using(CookingType_Menu::class);
    }
}
