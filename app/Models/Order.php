<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Order extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'status',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $with = [
        'menus',
    ];

    public static function status(){
        return [
            0 => 'Ожидание',
            1 => 'Принят в работу',
            2 => 'Выполнен',
        ];
    }
    
    public function getReadableStatusAttribute(){
        return $this->status()[$this->status];
    }

    public function menus(){
        return $this->belongsToMany(Menu::class, 'menu__order')->withPivot('total_price','count','cook_id')->using(Menu_Order::class);
    }
}
