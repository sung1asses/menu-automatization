<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class Cook extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $guarded = [
        'id'
    ];

    protected $allowedFilters = [
        'id',
        'name',
        'is_present',
        'created_at',
        'updated_at',
    ];
    
    protected $allowedSorts = [
        'id',
        'name',
        'is_present',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'is_present' => 'boolean',
    ];

    protected $with = [
    ];

    public function getProffessionAttribute(){
        return $this->cooking_type->title;
    }

    public function presence(){
        return [
            0 => 'Отсутствует',
            1 => 'Присутствует',
        ];
    }

    public function getPresenceAttribute(){
        return $this->presence()[$this->is_present];
    }

    public function cooking_type(){
        return $this->belongsTo(CookingType::class);
    }
}
