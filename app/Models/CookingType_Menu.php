<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CookingType_Menu extends Pivot
{
    public $incrementing = true;
}
